#ifndef XMLDERICHSINGLESOLIDRADIATORCNV_CPP
#define XMLDERICHSINGLESOLIDRADIATORCNV_CPP 1

// Include files
#include "DetDescCnv/XmlUserDetElemCnv.h"
#include "RichDet/DeRichSingleSolidRadiator.h"

typedef XmlUserDetElemCnv< DeRichSingleSolidRadiator > XmlDeRichSingleSolidRadiatorCnv;
DECLARE_CONVERTER( XmlDeRichSingleSolidRadiatorCnv )

#endif // XMLDERICHSINGLESOLIDRADIATORCNV_CPP
