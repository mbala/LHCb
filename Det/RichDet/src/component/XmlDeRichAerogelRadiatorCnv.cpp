#ifndef XMLDERICHAEROGELRADIATORCNV_H
#define XMLDERICHAEROGELRADIATORCNV_H 1

// Include files
#include "DetDescCnv/XmlUserDetElemCnv.h"
#include "RichDet/DeRichAerogelRadiator.h"

typedef XmlUserDetElemCnv< DeRichAerogelRadiator > XmlDeRichAerogelRadiatorCnv;
DECLARE_CONVERTER( XmlDeRichAerogelRadiatorCnv )

#endif // XMLDERICHAEROGELRADIATORCNV_H
