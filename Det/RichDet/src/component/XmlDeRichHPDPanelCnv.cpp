#ifndef XMLDERICHHPDPANELCNV_CPP
#define XMLDERICHHPDPANELCNV_CPP 1

// Include files
#include "DetDescCnv/XmlUserDetElemCnv.h"
#include "RichDet/DeRichHPDPanel.h"

typedef XmlUserDetElemCnv< DeRichHPDPanel > XmlDeRichHPDPanelCnv;
DECLARE_CONVERTER( XmlDeRichHPDPanelCnv )

#endif // XMLDERICHHPDPANELCNV_CPP
