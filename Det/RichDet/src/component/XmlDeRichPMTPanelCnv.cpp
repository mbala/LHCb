#ifndef XMLDERICHPMTPANELCNV_CPP
#define XMLDERICHPMTPANELCNV_CPP 1

// Include files
#include "DetDescCnv/XmlUserDetElemCnv.h"
#include "RichDet/DeRichPMTPanel.h"

typedef XmlUserDetElemCnv< DeRichPMTPanel > XmlDeRichPMTPanelCnv;
DECLARE_CONVERTER( XmlDeRichPMTPanelCnv )

#endif // XMLDERICHPMTPANELCNV_CPP
