#ifndef XMLDERICHMULTISOLIDRADIATORCNV_CPP
#define XMLDERICHMULTISOLIDRADIATORCNV_CPP 1

// Include files
#include "DetDescCnv/XmlUserDetElemCnv.h"
#include "RichDet/DeRichMultiSolidRadiator.h"

typedef XmlUserDetElemCnv< DeRichMultiSolidRadiator > XmlDeRichMultiSolidRadiatorCnv;
DECLARE_CONVERTER( XmlDeRichMultiSolidRadiatorCnv )

#endif // XMLDERICHMULTISOLIDRADIATORCNV_CPP
