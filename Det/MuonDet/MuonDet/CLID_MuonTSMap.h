#ifndef MUONDET_CLID_MUONTSMAP_H 
#define MUONDET_CLID_MUONTSMAP_H 1
#include "GaudiKernel/ClassID.h"

/// Class ID of the Muon readout
static const CLID CLID_MuonTSMap = 11093;  

#endif // MUONDET_CLID_MUONTSMAP_H
