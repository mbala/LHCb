
//-----------------------------------------------------------------------------
/** @file RichZeroSuppData.h
 *
 *  Header file for RICH DAQ utility class : RichZeroSuppData
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2004-12-17
 */
//-----------------------------------------------------------------------------

#pragma once

#include "RichDAQKernel/RichZeroSuppData_V1.h"
#include "RichDAQKernel/RichZeroSuppData_V2.h"
#include "RichDAQKernel/RichZeroSuppData_V3.h"
#include "RichDAQKernel/RichZeroSuppData_V4.h"
