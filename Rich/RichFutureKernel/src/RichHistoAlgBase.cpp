
//-----------------------------------------------------------------------------
/** @file RichHistoAlgBase.cpp
 *
 *  Implementation file for class : RichHistoAlgBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2002-04-05
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureKernel/RichHistoAlgBase.h"

// ============================================================================
// Force creation of templated class
#include "RichHistoBase.icpp"
template class Rich::Future::CommonBase< GaudiHistoAlg >;
template class Rich::Future::HistoBase< GaudiHistoAlg >;
// ============================================================================
