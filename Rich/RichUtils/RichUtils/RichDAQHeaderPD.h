
//=============================================================================================
/** @file RichDAQHeaderPD.h
 *
 *  Header file to include all the various HPD header classes
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   2003-11-06
 */
//=============================================================================================

#pragma once

#include "RichUtils/RichDAQHeaderPD_V1.h"
#include "RichUtils/RichDAQHeaderPD_V2.h"
#include "RichUtils/RichDAQHeaderPD_V3.h"
#include "RichUtils/RichDAQHeaderPD_V4.h"

//============================================================================================
