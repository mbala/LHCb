
#ifndef LHCbKernel_RichSmartID32_H
#define LHCbKernel_RichSmartID32_H 1

// Local
#include "Kernel/RichSmartID.h"

namespace LHCb
{
  // Just an alias to the main class, which is 32 bits
  using RichSmartID32 = RichSmartID;
} // namespace LHCb

#endif // LHCbKernel_RichSmartID32_H
