

  // Forward declarations2
  @@@forwardDeclsLHCb@@@
  @@@classID@@@
  @@@classVersion@@@
  @@@locations@@@

  /** @class @@@classname@@@ @@@classname@@@.h
   *
   * @@@desc@@@
   *
   * @author @@@author@@@
   * created @@@today@@@
   *
   */

  class @@@classname@@@@@@final@@@@@@inheritance@@@
  {
  public:

  @@@classContainerTypedefs@@@
  @@@publicTypedefs@@@
  @@@publicEnums@@@
  @@@constructorDecls@@@
  @@@destructorDecl@@@
  @@@classIDDecl@@@
  @@@streamerDecl@@@
  @@@enumConversionDecls@@@
  @@@publicMethodDecls@@@
  @@@getSetMethodDecls@@@
  @@@publicBitfieldEnums@@@
  @@@publicAttributes@@@
  @@@allocatorOperators@@@
  protected:

  @@@protectedTypedefs@@@
  @@@protectedEnums@@@
  @@@protectedMethodDecls@@@
  @@@protectedBitfieldEnums@@@
  @@@protectedAttributes@@@
  private:

  @@@privateTypedefs@@@
  @@@privateEnums@@@
  @@@privateMethodDecls@@@
  @@@privateBitfieldEnums@@@
  @@@privateAttributes@@@
  @@@enumConversionMaps@@@
  }; // class @@@classname@@@

  @@@classTypedefs@@@
  @@@globalTypedefs@@@
  @@@classOstreamOverload@@@
  @@@enumOstreamOverloads@@@

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations
@@@forwardIncludes@@@

@@@constructorDefs@@@
@@@destructorDef@@@
@@@classIDDef@@@
@@@streamerDef@@@
@@@enumConversionDefs@@@
@@@getSetMethodDefs@@@
@@@publicMethodDefs@@@
@@@protectedMethodDefs@@@
@@@privateMethodDefs@@@

@@@registerAllocatorReleaser@@@
